<?php

/* 
Author: Sergey Artikbayev https://vk.com/omut00 omut-00@mail.ru
 */

return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        '<controller>/<action>' => '<controller>/<action>',
        //'user/<controller>/<action>' => '/artmix/User/controllers/backend/<controller>/<action>',
        //'user/default/index' => 'artmix/User/backend//<action>',
        //'<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
        '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
        '/' => 'main/site/index',
    ],
];

