<?php

/* 
Author: Sergey Artikbayev https://vk.com/omut00 omut-00@mail.ru
 */

namespace backend\models;

use yii\db\ActiveRecord;

class Table extends ActiveRecord
{
    /*
     * Подключение таблицы
     */
    public function join($data)
    {
        $this->name = $data->name;
        $this->insert();
    }
    
    /*public function getTables()
    {
        return $this->find()->all();
    }*/
}