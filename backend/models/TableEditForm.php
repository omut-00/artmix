<?php

/* 
Author: Sergey Artikbayev https://vk.com/omut00 omut-00@mail.ru
 */

namespace backend\models;

use yii\base\Model;

class TableEditForm extends Model
{
    public $column;
    public $field;

    public function rules()
    {
        return [
            [['column'], 'required'],
            [['field'], 'required'],
        ];
    }
}
