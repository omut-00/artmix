<?php

/* 
Author: Sergey Artikbayev https://vk.com/omut00 omut-00@mail.ru
 */

namespace backend\models;

use Webmozart\Assert\Assert;
use yii\db\ActiveRecord;

class UserNetworks extends ActiveRecord
{
    public static function create($network, $identity): self
    {
        Assert::notEmpty($network);
        Assert::notEmpty($identity);
        
        $item = new static();
        $item->network = $network;
        $item->identity = $identity;
        return $item;
    }
    
    public static function tableName()
    {
        return '{{%user_networks}}';
    }
}