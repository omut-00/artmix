<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class CommonAsset extends AssetBundle
{
    public $sourcePath = '@common/static';
    public $css = [
        
    ];
    public $js = [
        'js/ar.js',
    ];
    public $depends = [
        
    ];
}
