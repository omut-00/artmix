<?php

/* 
Author: Sergey Artikbayev https://vk.com/omut00 omut-00@mail.ru
 */

namespace common\components;
/**
 * Interface ModuleInterface
 * @package app\components
 */
interface ModuleInterface
{
  /**
   * Initializes the module.
   */
  public function init();
  /**
   * Bootstrap method to be called during application bootstrap stage.
   * @param Application $app the application currently running
   */
  public function bootstrap($app);
}