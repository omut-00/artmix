<?php

/* 
Author: Sergey Artikbayev https://vk.com/omut00 omut-00@mail.ru
 */

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\BootstrapInterface;

class ModuleManager extends Component implements BootstrapInterface
{
    public function bootstrap($app)
    {
        /*$folder = (Yii::$app->id === 'app-backend') ? 'backend' : 'frontend';
        
        Yii::$app->setModule('user', [
            'class' => 'modules\user\\'. $folder .'\User'
        ]);*/
        
        $modules = [/*'site', 'user'*/'main', 'shop'];
        
        foreach($modules as $module){
            Yii::$app->setModule($module, [
                'class' => 'modules\\'. $module .'\Module'
            ]);
            Yii::$app->getModule($module)->bootstrap(Yii::$app);
        }
    }
}