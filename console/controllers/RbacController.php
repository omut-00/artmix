<?php

/* 
Author: https://vk.com/omut00 omut-00@mail.ru
 */

namespace console\controllers;

use Yii;
use yii\console\Controller;
/**
 * Инициализатор RBAC выполняется в консоли php yii rbac/init
 */
class RbacController extends Controller {

    public function actionInit() {
        
    }
}