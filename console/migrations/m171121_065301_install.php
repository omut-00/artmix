<?php

use yii\db\Migration;
use modules\main\models\UserAuth;

class m171121_065301_install extends Migration
{
    public function safeUp()
    {
        $model = UserAuth::find()->where(['username' => 'admin'])->one();
        if (empty($model)) {
            $user = new UserAuth();
            $user->username = 'admin';
            $user->email = 'omut-00@mail.ru';
            $user->setPassword('admin');
            $user->generateAuthKey();
            if ($user->save()) {
                echo 'User admin add';
            }
        }
        
        $auth = \Yii::$app->authManager;
        
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        // assign admin user with ID 1
        $auth->assign($admin, 1);
    }

    public function safeDown()
    {
        $auth = \Yii::$app->authManager;
        $admin = $auth->getRole('admin');
        if($admin){
            $auth->remove($admin);
        }
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171120_222426_install cannot be reverted.\n";

        return false;
    }
    */
}
