<?php

/* 
Author: Sergey Artikbayev https://vk.com/omut00 omut-00@mail.ru
 */

return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'enableStrictParsing' => true,
    'baseUrl' => '',
    'rules' => [
        //'<controller>/<action>' => '<controller>/<action>',
        //'<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
        '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
        '/' => 'main/site/index',
    ],
];

