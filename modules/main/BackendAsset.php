<?php

/**
 * @author Sergey Artikbayev <https://vk.com/omut00> <omut-00@mail.ru>
 */

namespace modules\main;

use yii\web\AssetBundle;

class BackendAsset extends AssetBundle
{
    
    //public $basePath = '@modules/main/web/backend';
    //public $baseUrl = '@web';
    
    public $sourcePath = '@modules/main/web/backend';
    public $css = [
        'css/site.css',
        'bootflat/css/bootflat.min.css'
    ];
    public $js = [
        'bootflat/js/icheck.min',
        'bootflat/js/jquery.fs.selecter.min',
        'bootflat/js/jquery.fs.stepper.min',
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'dmstr\web\AdminLteAsset'
    ];
}
