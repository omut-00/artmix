<?php

/* 
Author: Sergey Artikbayev https://vk.com/omut00 omut-00@mail.ru
 */

namespace modules\main;

use common\components\ModuleInterface;
use yii\web\GroupUrlRule;
use Yii;

class Module extends \common\components\Module implements ModuleInterface
{
    public static $name = 'main';
    
    /**
     * Initializes the module.
     */
    public function init()
    {
        parent::init();
    }
    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param \app\components\Application $app
     */
    public function bootstrap($app)
    {
        /*$app->get('frontendUrlManager')->rules[] = new GroupUrlRule([
            'prefix' => 'main',
            'rules' => [
                '<username:\w+>' => 'user/index',
            ],
        ]);*/
        
        $app->get('frontendUrlManager')->addRules([
            'user/<username:\w+>' => 'main/user/index'
        ], false);
        
        /*$app->getUrlManager()->addRules([
            //'user' => 'user/default/index',
        ],false);*/
        
        $app->i18n->translations['user'] = $app->i18n->translations['site'] = $app->i18n->translations['language'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@modules/main/messages',
        ];
        
        if (YII_ENV_DEV) {
            //$app->get('assetManager')->forceCopy = true;
        }
        
        parent::bootstrap($app);
    }
    
    public function backendBootstrap($app)
    {
        // добавление пункта меню
        /*\yii\base\Event::on(\dmstr\widgets\Menu::className(), \dmstr\widgets\Menu::EVENT_BEFORE_RUN, function ($event) {
            $event->sender->items[] = ['label' => Yii::t('user', 'Users'), 'icon' => 'shopping-cart', 'url' => ['user/index']];
        });*/
        \modules\main\models\MenuService::addItems('admin_left_sidebar', [
            ['priority' => 10, 'label' => Yii::t('user', 'Users'), 'icon' => 'user-o', 'url' => '#', 'items' => [
                ['priority' => 1, 'label' => Yii::t('user', 'All'), 'icon' => 'group', 'url' => ['/main/user/index']]
            ]],
            ['priority' => 1, 'label' => Yii::t('user', 'System'), 'icon' => 'cog', 'url' => '#', 'items' => [
                ['label' => Yii::t('user', 'Localisation'), 'icon' => 'language', 'url' => '#', 'items' => [
                    ['label' => Yii::t('user', 'Languages'), 'icon' => 'language', 'url' => ['/main/language/index']]
                ]],
            ]],
        ]);
        
        // bootflat
        
        //$app->registerCssFile('path/to/myfile');
        
        //\modules\main\BackendAsset::register($this);
    }
    
    public function frontendBootstrap($app)
    {
        
    }
    
    /*public static function t($category, $message, $params = [], $language = null)
    {
        return \Yii::t('' . $category, $message, $params, $language);
    }*/
}