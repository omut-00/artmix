<?php

/**
 * @author Sergey Artikbayev <https://vk.com/omut00> <omut-00@mail.ru>
 */

namespace modules\main\behaviors;

use yii\behaviors\SluggableBehavior as SB;
use yii\helpers\Inflector;
use dosamigos\transliterator\TransliteratorHelper;

class SluggableBehavior extends SB
{
    protected function generateSlug($slugParts)
    {
        return Inflector::slug(TransliteratorHelper::process(implode('-', $slugParts)), '-', true);
    }
}