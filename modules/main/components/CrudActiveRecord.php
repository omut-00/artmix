<?php

/**
 * @author Sergey Artikbayev <https://vk.com/omut00> <omut-00@mail.ru>
 * Active Record for Multilanguage and Statuses
 */

namespace modules\main\components;

use yii;

class CrudActiveRecord extends \yii\db\ActiveRecord
{
    use \modules\main\traits\MultilanguageParentTrait;
    
    const STATUS_ENABLED = '1';
    const STATUS_DISABLED = '0';
    
    const SCENARIO_CREATE_IN_ADMIN = 'createInAdmin';
    const SCENARIO_UPDATE_IN_ADMIN = 'updateInAdmin';
    
    public static function find($q = null)
    {
        return parent::find($q)->andWhere(['status' => self::STATUS_ENABLED]);
    }
    
    /*public function transactions()
    {
        //parent::transactions();
         return [
            //'admin' => self::OP_INSERT,
            //'api' => self::OP_INSERT | self::OP_UPDATE | self::OP_DELETE,
            // вышеприведённая строка эквивалентна следующей:
            self::SCENARIO_CREATE_IN_ADMIN => self::OP_ALL,
            self::SCENARIO_UPDATE_IN_ADMIN => self::OP_ALL,
        ];
    }*/
    
    public function load($data, $formName = null): bool
    {
        return parent::load($data, $formName) &&
                $this->loadTexts($data);
    }
    
    public function validate($attributeNames = null, $clearErrors = true): bool
    {
        return parent::validate($attributeNames, $clearErrors) &&
                $this->validateTexts($attributeNames);
    }
    
    public function save($runValidation = true, $attributeNames = null): bool
    {
        $transaction = Yii::$app->db->beginTransaction();
            
        try{
            $saveSelf = parent::save($runValidation, $attributeNames);
            $this->saveTexts();

            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollBack();
        }

        return $saveSelf/* && $saveText*/;
    }
}