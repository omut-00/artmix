<?php

/* 
Author: https://vk.com/omut00 omut-00@mail.ru
 */

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\TableJoinForm;
use backend\models\TableEditForm;
use backend\models\Table;
use yii\data\Pagination;


/**
 * Контроллер таблиц
 */
class TableController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['add', 'index', 'join', 'edit'],
                        'allow' => true,
                        'roles' => ['super_admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add' => ['get'],
                    'join' => ['get', 'post'],
                    'edit' => ['get', 'post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        /*$table = new Table();
        $tables = $table->getTables();*/
        
        $query = Table::find();
        
        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $tables = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        
        return $this->render('index',[
            'tables' => $tables,
            'pagination' => $pagination,
        ]);
    }

    public function actionAdd()
    {
        return $this->render('index');
    }
    
    public function actionEdit()
    {
        $form = new TableEditForm();
        $table = new Table();
        
        $request = Yii::$app->request;
        
        if(is_numeric($table_id = $request->get('id'))){
            Yii::$app->db->createCommand()
                    ->select("COLUMN_NAME")
                    ->where("table_name = (SELECT name FROM {%table} WHERE id = :table_id) AND table_schema = :table_schema", [
                        'table_name' => '',
                        'table_schema' => '',
                    ])
                    -> execute();

            if ($form->load(Yii::$app->request->post()) && $form->validate()) {

                return $this->render('index');
            } else {
                // либо страница отображается первый раз, либо есть ошибка в данных
                return $this->render('edit', ['model' => $form]);
            }
        }
    }
    
    public function actionJoin()
    {
        
        $form = new TableJoinForm();
        $table = new Table();
        
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            $table->join($form);
 
            return $this->render('index');
        } else {
            // либо страница отображается первый раз, либо есть ошибка в данных
            return $this->render('join', ['model' => $form]);
        }
    }
   
}