<?php

namespace modules\main\controllers\backend;

use Yii;
use yii\web\Controller;
use modules\main\models\User;
use modules\main\models\UserSearch;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $userSearch = new UserSearch();
        $dataProvider = $userSearch->search(Yii::$app->request->get());
        
        return $this->render('index', compact('userSearch', 'dataProvider'));
    }
    
    public function actionUpdate($id)
    {
        $user = User::findModel($id);
        $user->setScenario("update_in_admin");
 
        if ($user->load(Yii::$app->request->post()) && $user->validate()) {
            $user->save(false);
            return $this->redirect(['index']);
        }
        // render
        return $this->render('update', compact('user'));
    }
    
    public function actionDelete($id)
    {
        if ($id == \Yii::$app->user->getId()) {
            Yii::$app->getSession()->setFlash('danger', Yii::t('user', 'You can not remove your own account'));
        } else {
            $model = User::findModel($id);
            $model->delete();
            Yii::$app->getSession()->setFlash('success', Yii::t('user', 'User has been deleted'));
        }
        return $this->redirect(['index']);
    }
}
