<?php

namespace modules\main\controllers\frontend;

use yii\web\Controller;
use modules\main\models\User;

/**
 * Default controller for the `product` module
 */
class UserController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($username)
    {
        $user = User::findModel(['username' => $username]);
        
        return $this->render('index', [
            'user' => $user,
        ]);
    }
    /*public function findModel()
    {    
        if ($user) {
            return $user;
        }
        
        
    }*/
}
