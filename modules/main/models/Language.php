<?php

namespace modules\main\models;

use Yii;

/**
 * This is the model class for table "{{%language}}".
 *
 * @property string $language_id
 * @property string $language
 * @property string $country
 * @property string $name
 * @property string $name_ascii
 * @property integer $status
 */
class Language extends \yii\db\ActiveRecord
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%language}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id', 'language', 'country', 'name', 'name_ascii', 'status'], 'required'],
            [['status'], 'integer'],
            [['language_id'], 'string', 'max' => 5],
            [['language', 'country'], 'string', 'max' => 3],
            [['name', 'name_ascii'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'language_id' => Yii::t('language', 'Language ID'),
            'language' => Yii::t('language', 'Language'),
            'country' => Yii::t('language', 'Country'),
            'name' => Yii::t('language', 'Name'),
            'name_ascii' => Yii::t('language', 'Name Ascii'),
            'status' => Yii::t('language', 'Status'),
        ];
    }
    
}
