<?php

/**
 * @author Sergey Artikbayev <https://vk.com/omut00> <omut-00@mail.ru>
 */

namespace modules\main\models;

class MenuService
{
    private static $items = [];
    
    /**
     * Add item to menu
     * @param string $menuName
     * @param array $items for widget menu
     */
    public static function addItems(string $menuName, array $items): void
    {
        if(!isset(static::$items[$menuName])) static::$items[$menuName] = [];
        static::$items[$menuName] = array_merge_recursive(static::$items[$menuName], $items);
    }
    
    public static function getItems(string $menuName, array $staticItems = []): array
    {
        $menuItems = (isset(static::$items[$menuName])) ? static::$items[$menuName] : [];
        $items = array_merge_recursive($staticItems, $menuItems);
        
        static::sortItems($items);

        return $items;
    }
    
    private static function sortItems(array &$items): void
    {
        \yii\helpers\ArrayHelper::multisort($items, 'priority', SORT_DESC);

        foreach($items as &$item){
            if(isset($item['items'])){
                static::sortItems($item['items']);
            }
        }
    }
}

