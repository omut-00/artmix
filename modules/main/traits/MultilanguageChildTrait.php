<?php

/**
 * @author Sergey Artikbayev <https://vk.com/omut00> <omut-00@mail.ru>
 */

namespace modules\main\traits;

use modules\main\models\Language;
use Yii;

trait MultilanguageChildTrait
{
    public function defaultRules(array $attributes = []): array
    {
        $defaultRules = [
            'name' => [
                [['name'], 'required'],
                [['name'], 'string', 'max' => 255],
            ],
            'description' => [
                [['description'], 'string'],
            ],
            'language_id' => [
                [['language_id'], 'required'],
                [['language_id'], 'string', 'max' => 5],
                [['language_id'], 'exist', 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'language_id']],
            ],
            'meta_title' => [
                [['meta_title'], 'string', 'max' => 200],
            ],
            'meta_description' => [
                [['meta_description'], 'string', 'max' => 200],
            ],
            'meta_keyword' => [
                [['meta_keyword'], 'string', 'max' => 200],
            ]
        ];
        
        $returnDefaultRules = [];
        
        foreach($attributes as $attribute){
            if(isset($defaultRules[$attribute])){
                foreach($defaultRules[$attribute] as $rule){
                    $returnDefaultRules[] = $rule;
                }
            }
        }
        
        return $returnDefaultRules;
    }
    
    public function defaultAttributeLabels(array $attributes = []): array
    {
        //$language_id = 
        
        $defaultAttributeLables = [
            'language_id' => Yii::t('shopCategory', 'Language ID'),
            'name' => Yii::t('shopCategory', 'Name'),
            'description' => Yii::t('shopCategory', 'Description'),
            'meta_title' => Yii::t('shopCategory', 'Meta Title'),
            'meta_description' => Yii::t('shopCategory', 'Meta Description'),
            'meta_keyword' => Yii::t('shopCategory', 'Meta Keyword'),
        ];
        
        $returnDefaultAttrLabels = [];
        
        foreach($attributes as $attribute){
            if(isset($defaultAttributeLables[$attribute])){
                $returnDefaultAttrLabels[$attribute] = $defaultAttributeLables[$attribute];
            }
        }
        
        return $returnDefaultAttrLabels;
    }
    
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['language_id' => 'language_id'])->andWhere('status = :status', [
            ':status' => Language::STATUS_ENABLED
        ]);
    }
    public function setLanguage($val)
    {
        $this->language = $val;
    }
    
    public function loadDefaultAttrText(){
        $this->language_id = $this->language->language_id;
    }
}