<?php

/**
 * @author Sergey Artikbayev <https://vk.com/omut00> <omut-00@mail.ru>
 */

namespace modules\main\traits;

use modules\main\models\Language;
use Yii;

trait MultilanguageParentTrait
{
    
    public $stackTexts = [];
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTexts()
    {
        return $this->hasMany($this->textOptions['class'], [$this->textOptions['relationField'] => 'id']);
    }
    
    public function setTexts($val){
        $this->texts = $val;
    }
    
    public function getText()
    {
        return $this->hasOne($this->textOptions['class'], [$this->textOptions['relationField'] => 'id'])
                ->onCondition(['language_id' => Yii::$app->language])
                //->andWhere("language_id = :language_id", [':language_id' => Yii::$app->language])
                /*->from(['ct' => Text::tableName()])*/;
    }
    
    public function setText($val){
        $this->text = $val;
    }
    
    /**
     * Get texts with enabled languages and Create text if is not set text for new languages
     */
    public function getStackTexts()
    {
        $areTexts = \yii\helpers\ArrayHelper::index($this->texts, 'language_id');
        $stackTexts = [];
        foreach($this->languages as $language){
            if(isset($areTexts[$language->language_id])){
                $stackTexts[$language->language_id] = $areTexts[$language->language_id];
            }else{
                $stackTexts[$language->language_id] = new $this->textOptions['class'];
            }
            $stackTexts[$language->language_id]->scenario = $this->scenario;
            $stackTexts[$language->language_id]->language = $language;
        }

        return $stackTexts;
    }
    
    public function getLanguages()
    {
        return Language::find()->where(['status' => Language::STATUS_ENABLED])->all();
    }
    
    public function loadTexts($data): bool
    {
        $load = \yii\base\Model::loadMultiple($this->stackTexts, $data);
        $this->text = $this->stackTexts[Yii::$app->language];
        
        return $load;
    }
    
    public function validateTexts($attributeNames = null): bool
    {
        return \yii\base\Model::validateMultiple($this->stackTexts, $attributeNames);
    }
    
    public function saveTexts(): bool
    {
        foreach($this->stackTexts as $text){
            $saveText = $this->link('texts', $text) && isset($saveText) ? $saveText : true;
        }
        
        return isset($saveText) ? $saveText : false;
    }
}