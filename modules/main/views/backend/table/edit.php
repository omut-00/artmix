<?php

/* 
Author: Sergey Artikbayev https://vk.com/omut00 omut-00@mail.ru
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'id' => 'table-edit',
    'options' => ['class' => 'form-horizontal'],
]) ?>
<?= $form->field($model, 'column')->dropdownList(
    ProductCategory::find()->select(['category_name', 'id'])->indexBy('id')->column(),
    ['prompt'=>'Select Category']
) ?>
<?= $form->field($model, 'field') ?>
<div class="form-group">
    <div class="">
        <?= Html::submitButton('Ok', ['class' => 'btn btn-primary']) ?>
    </div>
</div>
<?php ActiveForm::end() ?>

