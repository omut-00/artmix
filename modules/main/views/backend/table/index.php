<?php

/* 
Author: https://vk.com/omut00 omut-00@mail.ru
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;

?>
<a href="<?php echo Url::to(['table/add']); ?>"><button class="btn">Создать таблицу</button></a>
<a href="<?php echo Url::to(['table/join']); ?>"><button class="btn">Подключить таблицу</button></a>

<h1>Таблицы</h1>
<ul>
<?php foreach ($tables as $table){ ?>
    <li>
        <a href="<?php echo Url::to(['table/edit', 'id' => $table->id]); ?>"><?= Html::encode($table->name) ?></a>
    </li>
<?php } ?>
</ul>

<?= LinkPager::widget(['pagination' => $pagination]) ?>

