<?php

use yii\grid\GridView;
use yii\helpers\Html;
//use yii\helpers\Url;
//use yii\web\View;

$this->title = Yii::t('user', 'Manage users');
$this->params['breadcrumbs'][] = $this->title;
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $userSearch,
    'columns' => [
        'id',
        'username',
        'email:email',
        'created_at',
        [
            'class' => 'yii\grid\ActionColumn',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    $url = Yii::$app->frontendUrlManager->createUrl(['main/user/index', 'username' => $model->username]);
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['target' => '_blank']);
                },
            ],
        ],
    ],
]); ?>