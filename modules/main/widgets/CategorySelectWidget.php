<?php

/**
 * @author Sergey Artikbayev <https://vk.com/omut00> <omut-00@mail.ru>
 */

namespace modules\main\widgets;

use yii\base\Widget;
use yii\helpers\ArrayHelper;

class CategorySelectWidget extends Widget
{
    //public $categoryList = [];
    public $model;
    public $attribute;
    public $items;
    public $multiple = false;
    public $value = false;
    public $tags = false;

    public function run()
    {
        $items = ArrayHelper::index($this->items, 'id');

        foreach($items as &$item){
            $item['treeName'] = $item['name'] . static::treeName($items, $item);
        }

        return \kartik\select2\Select2::widget([
            'model' => $this->model,
            'attribute' => $this->attribute,
            'data' => ArrayHelper::map($items, 'id', 'treeName'),
            'options' => [
                'placeholder' => 'Select a category ...',
                'value' => $this->value,
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => $this->multiple,
                'tags' => $this->tags,
            ],
        ]);
    }
    
    /*private function indexingList($items)
    {
        if(!$this->items){
            $this->items = ArrayHelper::index($items, 'id');
        }
        return $this->items;
    }*/
    
    private static function treeName($items, $item)
    {
        if($item['parent_id']){
            return ' < '.$items[$item['parent_id']]['name'] . static::treeName($items, $items[$item['parent_id']]);
        }
    }
    
    /*public function treeNameParent($items, $parent_id)
    {
        if($parent_id){
            $items = $this->indexingList($items);
            return static::treeName($items, $items[$parent_id]);
        }
    }*/
}