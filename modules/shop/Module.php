<?php

/* 
Author: Sergey Artikbayev https://vk.com/omut00 omut-00@mail.ru
 */

namespace modules\shop;

use common\components\ModuleInterface;
//use yii\web\GroupUrlRule;
use Yii;

class Module extends \common\components\Module implements ModuleInterface
{
    public static $name = 'shop';
    
    /**
     * Initializes the module.
     */
    public function init()
    {
        parent::init();
    }
    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param \app\components\Application $app
     */
    public function bootstrap($app)
    {
        
        $app->i18n->translations['shopProduct'] = $app->i18n->translations['shopCategory'] = $app->i18n->translations['shopProducer'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@modules/shop/messages',
        ];
        
        parent::bootstrap($app);
    }
    
    public function backendBootstrap($app)
    {
        // add menu items
        \modules\main\models\MenuService::addItems('admin_left_sidebar', [
            ['priority' => 20, 'label' => Yii::t('shopProduct', 'Products'), 'icon' => 'shopping-cart', 'url' => ['/shop/product/index'], 'items' => [
                ['priority' => 20, 'label' => Yii::t('shopProduct', 'All'), 'icon' => 'shopping-cart', 'url' => ['/shop/product/index']],
                ['priority' => 15, 'label' => Yii::t('shopCategory', 'Categories'), 'icon' => 'code-fork', 'url' => ['/shop/category/index']],
                ['priority' => 10, 'label' => Yii::t('shopCategory', 'Manufacturers'), 'icon' => 'wrench', 'url' => ['/shop/manufacturer/index']],
            ]]
        ]);
    }
    
    public function frontendBootstrap($app)
    {
        // add menu items
        $categories = models\Category::find()->where(['parent_id' => null, 'status' => models\Category::STATUS_ENABLED])->joinWith('text')->all();

        foreach($categories as $category){
            if($category->text->name){
                $menuCategories[] = ['label' => $category->text->name . '1', 'icon' => 'shopping-cart', 'url' => ['/shop/category/view', 'slug' => $category->slug]];
            }
        }
        \modules\main\models\MenuService::addItems('header_menu', $menuCategories);
    }
}