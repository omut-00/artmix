<?php

namespace modules\shop\controllers\frontend;

class CategoryController extends \yii\web\Controller
{
    public function actionView()
    {
        return $this->render('view');
    }

}
