<?php

use yii\db\Schema;
use yii\db\Migration;

class m171121_065805_shop_mass extends Migration
{
     /*private $permissions = [
        // product
        'shopProductFull' => [
            'description' => 'Full access for products',
        ],
        'shopProductEdit' => [
            'description' => 'Create, Update, Delete access for products'
        ],
        'shopProductView' => [
            'description' => 'View access for products',
        ],
        // category
        'shopCategoryFull' => [
            'description' => 'Full access for categories',
        ],
        'shopCategoryEdit' => [
            'description' => 'Create, Update, Delete categories'
        ],
        'shopCategoryView' => [
            'description' => 'View categories',
        ],
        // manufacturer
        'shopManufacturerFull' => [
            'description' => 'Full access for manufacturers',
        ],
        'shopManufacturerEdit' => [
            'description' => 'Create, Update, Delete manufacturers'
        ],
        'shopManufacturerView' => [
            'description' => 'View manufacturers',
        ],
    ];*/
     
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        try {
            
            /**
             * Product
             */
            
            $this->createTable('{{%shop_product}}', [
                'id' => Schema::TYPE_PK . "",
                'manufacturer_id' => Schema::TYPE_INTEGER . "(11)",
                'quantity' => Schema::TYPE_INTEGER . "(11) DEFAULT '0'",
                'model' => $this->string(64),

                //'code' => Schema::TYPE_STRING . "(155)",
                //'price' => Schema::TYPE_DECIMAL . "(11, 2)", test TYPE_MONEY
                'price' => Schema::TYPE_MONEY,
                /*'is_new' => "enum('yes','no')" . " DEFAULT 'no'",
                'is_popular' => "enum('yes','no')" . " DEFAULT 'no'",
                'is_promo' => "enum('yes','no')" . " DEFAULT 'no'",*/
                //'images' => Schema::TYPE_TEXT . "",
                'sku' => Schema::TYPE_STRING . '(64) NOT NULL DEFAULT \'\'',
                'status' => Schema::TYPE_SMALLINT . "(1) NOT NULL",
                'sort_order' => Schema::TYPE_INTEGER . "(3) NOT NULL DEFAULT '0'",
                'slug' => $this->string(255)->notNull()->unique(),
                
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
            ], $tableOptions);
            
            $this->createIndex('manufactuer_id', '{{%shop_product}}', 'manufacturer_id', 0);
            
            $this->createTable('{{%shop_product_text}}', [
                'product_id' => Schema::TYPE_INTEGER . "(11) NOT NULL",
                'language_id' => Schema::TYPE_STRING . '(5) NOT NULL',
                'name' => Schema::TYPE_STRING . "(255) NOT NULL",
                'description' => Schema::TYPE_TEXT . "",
                'meta_title' => Schema::TYPE_STRING . "(200) NOT NULL",
                'meta_description' => Schema::TYPE_STRING . "(200)",
                'meta_keyword' => Schema::TYPE_STRING . "(200)",
                'tags' => Schema::TYPE_STRING . "(200)",
            ], $tableOptions);
            
            $this->createIndex('product_id_and_language', '{{%shop_product_text}}', ['product_id', 'language_id'], 1);
            
            $this->addForeignKey(
                'fk_product_text', '{{%shop_product_text}}', 'product_id', '{{%shop_product}}', 'id', 'CASCADE', 'CASCADE'
            );
            
            /**
             * Category
             */
            
            $this->createTable('{{%shop_category}}', [
                'id' => Schema::TYPE_PK . "",
                'parent_id' => Schema::TYPE_INTEGER . "(11)",
                'slug' => $this->string(255)->notNull()->unique(),
                'image_id' => Schema::TYPE_INTEGER . "(11)",
                'sort_order' => Schema::TYPE_INTEGER . "(3) NOT NULL DEFAULT '0'",
                'status' => Schema::TYPE_SMALLINT . "(1) NOT NULL",
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
            ], $tableOptions);
            
            $this->createIndex('id', '{{%shop_category}}', 'id,parent_id', 0);
            
            $this->createTable('{{%shop_category_text}}', [
                'category_id' => Schema::TYPE_INTEGER . "(11) NOT NULL",
                'language_id' => Schema::TYPE_STRING . '(5) NOT NULL',
                'name' => Schema::TYPE_STRING . "(255) NOT NULL",
                'description' => Schema::TYPE_TEXT . "",
                'meta_title' => Schema::TYPE_STRING . "(200) NOT NULL",
                'meta_description' => Schema::TYPE_STRING . "(200)",
                'meta_keyword' => Schema::TYPE_STRING . "(200)",
                //'tags' => Schema::TYPE_STRING . "(200)",
            ], $tableOptions);
            
            $this->createIndex('category_id_and_language', '{{%shop_category_text}}', ['category_id', 'language_id'], 1);
            
            $this->addForeignKey(
                'fk_category_text', '{{%shop_category_text}}', 'category_id', '{{%shop_category}}', 'id', 'CASCADE', 'CASCADE'
            );
            
            $this->createTable('{{%shop_product_to_category}}', [
                'id' => Schema::TYPE_PK . "",
                'product_id' => Schema::TYPE_INTEGER . "(11) NOT NULL",
                'category_id' => Schema::TYPE_INTEGER . "(11) NOT NULL",
                ], $tableOptions);
            
            $this->createIndex('product_to_category', '{{%shop_product_to_category}}', ['product_id', 'category_id'], 1);
            
            $this->addForeignKey(
                'fk_cat_to_product', '{{%shop_product_to_category}}', 'product_id', '{{%shop_product}}', 'id', 'CASCADE', 'CASCADE'
            );
            $this->addForeignKey(
                'fk_cat_to_product_2', '{{%shop_product_to_category}}', 'category_id', '{{%shop_category}}', 'id', 'CASCADE', 'CASCADE'
            );
            
            /**
             * Currency
             */
            
            $this->createTable('{{%shop_currency}}', [
                'id' => $this->primaryKey(),
                'title' => $this->string(35)->notNull(),
                'code' => $this->string(3)->notNull(),
                'symbol_left' => $this->string(12)->notNull(),
                'symbol_right' => $this->string(12)->notNull(),
                'decimal_place' => $this->char(1)->notNull(),
                'value' => $this->double(15, 8)->notNull(),
                'status' => $this->smallInteger(1)->notNull(),
                'updated_at' => $this->integer()->notNull(),
            ], $tableOptions);
            
            $this->batchInsert('{{%shop_currency}}', [
                'title', 'code', 'symbol_left', 'symbol_right', 'decimal_place', 'value', 'status', 'updated_at'
            ], [
                ['Тенге', 'KZT', '', ' т.', '2', 1.00000000, 1, time()],
                ['Рубль', 'RUB', '', ' р.', '2', 0.18000000, 1, time()],
                ['US Dollar', 'USD', '$', '', '2', 0.00300000, 1, time()],
                ['Euro', 'EUR', '', '€', '2', 0.00260000, 1, time()]
            ]);
            
            /**
             * Manufacturer
             */
            
            $this->createTable('{{%shop_manufacturer}}', [
                'id' => Schema::TYPE_PK . "",
                'name' => $this->string(64)->notNull(),
                'image_id' => $this->integer(11),
                'slug' => $this->string(255)->notNull()->unique(),
                'sort_order' => $this->integer(3)->notNull()->defaultValue(0)
            ], $tableOptions);
            
            $this->createTable('{{%shop_manufacturer_text}}', [
                'manufacturer_id' => $this->integer(11)->notNull(),
                'language_id' => Schema::TYPE_STRING . '(5) NOT NULL',
                'description' => $this->text(),
                'meta_title' => $this->string(200),
                'meta_description' => $this->string(200),
                'meta_keyword' => $this->string(200),
            ], $tableOptions);
            
            $this->createIndex('manufacturer_id_and_language', '{{%shop_manufacturer_text}}', ['manufacturer_id', 'language_id'], 1);
            
            $this->addForeignKey(
                'fk_manufacturer', '{{%shop_product}}', 'manufacturer_id', '{{%shop_manufacturer}}', 'id', 'RESTRICT', 'RESTRICT'
            );
            
            
            /**
             * Rbac
             */
            
            /*$auth = \Yii::$app->authManager;
        
            // permissions
            foreach($this->permissions as $name => $data){
                $permission[$name] = $auth->createPermission($name);
                $permission[$name]->description = $data['description'];
                $auth->add($permission[$name]);
            }

            // get roles
            $admin = $auth->getRole('admin');

            // children
            $auth->addChild($admin, $permission['shopProductFull']);

            $auth->addChild($permission['shopProductFull'], $permission['shopProductEdit']);
            $auth->addChild($permission['shopProductEdit'], $permission['shopProductView']);


            $auth->addChild($admin, $permission['shopCategoryFull']);

            $auth->addChild($permission['shopCategoryFull'], $permission['shopCategoryEdit']);
            $auth->addChild($permission['shopCategoryEdit'], $permission['shopCategoryView']);


            $auth->addChild($admin, $permission['shopManufacturerFull']);

            $auth->addChild($permission['shopManufactuerFull'], $permission['shopManufacturerEdit']);
            $auth->addChild($permission['shopManufacturerEdit'], $permission['shopManufacturerView']);*/
            
        } catch (Exception $e) {
            echo 'Catch Exception ' . $e->getMessage() . ' ';
        }
    }

    public function safeDown()
    {
        try {
            
            /*$auth = \Yii::$app->authManager;
        
            foreach($this->permissions as $name => $data){
                $permission = $auth->getPermission($name);
                if($permission){
                    $auth->remove($permission);
                }
            }*/
            
            $this->dropTable('{{%shop_product_to_category}}');
            $this->dropTable('{{%shop_product_text}}');
            $this->dropTable('{{%shop_product}}');
            
            $this->dropTable('{{%shop_category_text}}');
            $this->dropTable('{{%shop_category}}');
            
            $this->dropTable('{{%shop_manufacturer_text}}');
            $this->dropTable('{{%shop_manufacturer}}');
            
            $this->dropTable('{{%shop_currency}}');
            
        } catch (Exception $e) {
            echo 'Catch Exception ' . $e->getMessage() . ' ';
        }
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171115_160324_mass cannot be reverted.\n";

        return false;
    }
    */
}
