<?php

namespace modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%shop_category}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $slug
 * @property integer $image_id
 * @property integer $sort_order
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */

class Category extends \modules\main\components\CrudActiveRecord
{
    public $textOptions = [
        'class' => '\modules\shop\models\CategoryText',
        'relationField' => 'category_id',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_category}}';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \modules\main\behaviors\SluggableBehavior::className(),
                'attribute' => 'text.name',
                'ensureUnique' => true
            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            
        ];
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE_IN_ADMIN] = $scenarios[self::SCENARIO_UPDATE_IN_ADMIN] = ['sort_order', 'status', 'slug', 'parent_id'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_id', 'sort_order', 'status', 'created_at', 'updated_at'], 'integer'],
            [['status', 'created_at', 'updated_at', 'sort_order'], 'required'],
            
            [['parent_id'], 'default', 'value' => null],
            [['parent_id'], 'integer'],
            [['parent_id'], 'validateNotThisID'],
            
            //[['slug'], 'required'],
            [['slug'], 'string', 'max' => 255],
            [['slug'], 'match', 'pattern' => '/^[\w\-]+$/u', 'message' => Yii::t('shopCategory', '{attribute} can contain only letters, numbers, and "_", "-"')],
            [['slug'], 'unique'],
        ];
    }
    
    /**
     * Protection against recursion
     * @param type $attribute
     * @param type $params
     * @param type $validator
     */
    public function validateNotThisID($attribute, $params, $validator){
        if(!$this->isNewRecord){
            if($this->id == $this->$attribute){
                $this->addError($attribute, Yii::t('shopCategory', '"{parent_id}" you don\'t slect this category as parent category', $this->attributeLabels()));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('shopCategory', 'ID'),
            'parent_id' => Yii::t('shopCategory', 'Parent Category'),
            'slug' => Yii::t('shopCategory', 'Slug'),
            'image_id' => Yii::t('shopCategory', 'Image ID'),
            'sort_order' => Yii::t('shopCategory', 'Sort Order'),
            'status' => Yii::t('shopCategory', 'Status'),
            'created_at' => Yii::t('shopCategory', 'Created At'),
            'updated_at' => Yii::t('shopCategory', 'Updated At'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /*public function getShopProductToCategories()
    {
        return $this->hasMany(ShopProductToCategory::className(), ['category_id' => 'id']);
    }*/
    
    public function getList()
    {
        return static::find()->joinWith('text')
            ->select(['id', 'parent_id', CategoryText::getTableSchema()->fullName . '.name'])
            //->where(['language_id' => Yii::$app->language])
            ->asArray()
            ->all();
    }
    
    /*public function getCategoriesTree(&$tree = array(), $parentID = 0)
    {
        $criteria = new CDbCriteria;
        $criteria->condition='parent_id=:id';
        $criteria->params=array(':id'=>$parentID);
        $resultNumber = $this->count($criteria);

        if($resultNumber > 0)
        {
            $model = $this->findAll($criteria);
            // get category position as order_id
            $orderId = 0;
            foreach ($model as $key) 
            {
                $tree[$orderId] = array(
                    'id' => $key->id,
                    'parent_id' => $key->parent_id,
                    'name' => $key->name,
                );

                // get subcategories
                $this->getCategoriesTree($tree[$orderId]['subcat'], $key->id);

                if(count($tree[$orderId]['subcat']) === 0)
                {
                    unset($tree[$orderId]['subcat']);
                }

                $orderId++;
            }
        }
    }*/
}
