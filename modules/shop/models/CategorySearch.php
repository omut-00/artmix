<?php

namespace modules\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\shop\models\Category;

/**
 * CategorySearch represents the model behind the search form about `modules\shop\models\Category`.
 */
class CategorySearch extends Category
{
    //const SCENARIO_SEARCH_IN_ADMIN = 'search_in_admin';
    
    public function load($data, $formName = null): bool
    {
        return \yii\db\ActiveRecord::load($data, $formName);
    }
    
    public function validate($attributeNames = null, $clearErrors = true): bool
    {
        return \yii\db\ActiveRecord::validate($attributeNames, $clearErrors);
    }
    
    public function behaviors()
    {
        return[];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id',/* 'parent_id',*/ 'status', 'created_at', 'updated_at'], 'integer'],
            [['slug', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
        
        /*$scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_SEARCH_IN_ADMIN] = ['status', 'slug', 'name'];
        return $scenarios;*/
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function search($params)
    {
        $query = $this::find()->joinWith('text');
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $dataProvider->sort->attributes['name'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => [CategoryText::getTableSchema()->fullName . '.name' => SORT_ASC],
            'desc' => [CategoryText::getTableSchema()->fullName . '.name' => SORT_DESC],
        ];
        
        //$this->list = static::getList();

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'parent_id' => $this->parent_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
    
    //public $name;
    
    public function getName()
    {
        return $this->text->name;
    }
    
    public function setName($val)
    {
        return $this->name = $val;
    }
    
    /*public function getParentCategoryText()
    {
        return $this->hasOne(CategoryText::className(), ['category_id' => 'parent_id'])->andWhere("language_id = :language_id", [':language_id' => Yii::$app->language]);
    }
    
    public function getParentName()
    {
        return $this->parentCategoryText->name;
    }*/
}
