<?php

namespace modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%shop_category_text}}".
 *
 * @property integer $category_id
 * @property string $language
 * @property string $name
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 *
 * @property Category $category
 */
class CategoryText extends \yii\db\ActiveRecord
{
    use \modules\main\traits\MultilanguageChildTrait;
    
    public function load($data, $formName = null)
    {
        $this->loadDefaultAttrText();
        return parent::load($data, $formName);
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_category_text}}';
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[Category::SCENARIO_CREATE_IN_ADMIN] = $scenarios[Category::SCENARIO_UPDATE_IN_ADMIN] = ['name', 'description', 'meta_title', 'meta_description', 'meta_keyword'/*, '!category_id'*/, '!language_id'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge($this->defaultRules(['name', 'description', 'language_id', 'meta_title', 'meta_description', 'meta_keyword']), [
            [['category_id'], 'required'],
            [['category_id'], 'integer'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            
            [['category_id', 'language_id'], 'unique', 'targetAttribute' => ['category_id', 'language_id'], 'message' => 'The combination of Category ID and Language has already been taken.'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge($this->defaultAttributeLabels(['name', 'description', 'language_id', 'meta_title', 'meta_description', 'meta_keyword']), [
            'category_id' => Yii::t('shopCategory', 'Category ID'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
