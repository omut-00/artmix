<?php

namespace modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%shop_manufacturer}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $image_id
 * @property string $slug
 * @property integer $sort_order
 *
 * @property ShopProduct[] $shopProducts
 */
class Manufacturer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_manufacturer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['image_id', 'sort_order'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('shopManufacturer', 'ID'),
            'name' => Yii::t('shopManufacturer', 'Name'),
            'image_id' => Yii::t('shopManufacturer', 'Image ID'),
            'slug' => Yii::t('shopManufacturer', 'Slug'),
            'sort_order' => Yii::t('shopManufacturer', 'Sort Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopProducts()
    {
        return $this->hasMany(ShopProduct::className(), ['manufacturer_id' => 'id']);
    }
}
