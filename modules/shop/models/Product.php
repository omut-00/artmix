<?php

namespace modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%shop_product}}".
 *
 * @property integer $id
 * @property integer $manufacturer_id
 * @property integer $quantity
 * @property string $model
 * @property string $price
 * @property string $sku
 * @property integer $status
 * @property integer $sort_order
 * @property string $slug
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ShopManufacturer $manufacturer
 * @property ShopProductToCategory[] $shopProductToCategories
 * @property ShopProductTxt[] $shopProductTxts
 */
class Product extends \modules\main\components\CrudActiveRecord
{
    public $toCategoriesArray;
    
    public $textOptions = [
        'class' => '\modules\shop\models\ProductText',
        'relationField' => 'product_id',
    ];
    
    /**
	 * @var array Идентификаторы категории поста.
	 * Используется для получения ID категорий поста.
	 * Так же используется для определения выбраных категорий поста в момент его редактирования.
	 */
	//protected $_toCategories;
	/**
	 * @var array Текущие идентификаторы категории поста.
	 * Позволяет определить если категории поста были изменены.
	 */
	//protected $_oldToCategories;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_product}}';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \modules\main\behaviors\SluggableBehavior::className(),
                'attribute' => 'text.name',
                'ensureUnique' => true
            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE_IN_ADMIN] = $scenarios[self::SCENARIO_UPDATE_IN_ADMIN] = ['sort_order', 'status', 'slug', 'price', 'model', 'sku', 'toCategoriesArray'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacturer_id', 'quantity', 'status', 'sort_order', 'created_at', 'updated_at'], 'integer'],
            [['price'], 'number'],
            [['status', 'created_at', 'updated_at'], 'required'],
            [['model', 'sku'], 'string', 'max' => 64],
            [['slug'], 'string', 'max' => 255],
            [['manufacturer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Manufacturer::className(), 'targetAttribute' => ['manufacturer_id' => 'id']],
            
            [['toCategoriesArray'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('shopProduct', 'ID'),
            'manufacturer_id' => Yii::t('shopProduct', 'Manufacturer ID'),
            'quantity' => Yii::t('shopProduct', 'Quantity'),
            'model' => Yii::t('shopProduct', 'Model'),
            'price' => Yii::t('shopProduct', 'Price'),
            'sku' => Yii::t('shopProduct', 'Sku'),
            'status' => Yii::t('shopProduct', 'Status'),
            'sort_order' => Yii::t('shopProduct', 'Sort Order'),
            'slug' => Yii::t('shopProduct', 'Slug'),
            'created_at' => Yii::t('shopProduct', 'Created At'),
            'updated_at' => Yii::t('shopProduct', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(Manufacturer::className(), ['id' => 'manufacturer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToCategories()
    {
        return $this->hasMany(ProductToCategory::className(), ['product_id' => 'id']);
        /*return $this->hasMany(Category::className(), ['id' => 'category_id'])
            ->viaTable('{{%shop_product_to_category}}', ['product_id' => 'id']);*/
    }
    
    public function save($runValidation = true, $attributeNames = null): bool
    {
        $transaction = Yii::$app->db->beginTransaction();
            
        try{
            $toCategoryModels = [];
            if(!$this->isNewRecord){
                // delete relations if is removed
                ProductToCategory::deleteAll(['AND', 'product_id = :product_id', ['NOT IN', 'category_id', $this->toCategoriesArray]], [':product_id' => $this->id]);
                
                $indexToCAtegories = \yii\helpers\ArrayHelper::index($this->toCategories, 'category_id');
                
                foreach($this->toCategoriesArray as $toCategoryID){
                    $toCategoryModels[] = isset($indexToCAtegories[$toCategoryID]) ?
                        $indexToCAtegories[$toCategoryID] :
                        new ProductToCategory([
                            'category_id' => $toCategoryID,
                        ]);
                }
            }else{
                foreach($this->toCategoriesArray as $toCategoryID){
                    $toCategoryModels[] = new ProductToCategory([
                        'category_id' => $toCategoryID,
                    ]);
                }
            }
            
            $saveSelf = parent::save($runValidation, $attributeNames);

            foreach($toCategoryModels as $toCategoryModel){
                $this->link('toCategories', $toCategoryModel);
            }
            
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollBack();
        }

        return $saveSelf/* && $saveText*/;
    }
}
