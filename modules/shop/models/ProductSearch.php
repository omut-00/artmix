<?php

namespace modules\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\shop\models\Product;

/**
 * ProductSearch represents the model behind the search form about `modules\shop\models\Product`.
 */
class ProductSearch extends Product
{
    public function load($data, $formName = null): bool
    {
        return \yii\db\ActiveRecord::load($data, $formName);
    }
    
    public function validate($attributeNames = null, $clearErrors = true): bool
    {
        return \yii\db\ActiveRecord::validate($attributeNames, $clearErrors);
    }
    
    public function behaviors()
    {
        return[];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'manufacturer_id', 'quantity', 'status', 'sort_order', 'created_at', 'updated_at'], 'integer'],
            [['model', 'sku', 'slug', 'name'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this::find()->joinWith('text');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $dataProvider->sort->attributes['name'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => [ProductText::tableName().'.name' => SORT_ASC],
            'desc' => [ProductText::tableName() . '.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'manufacturer_id' => $this->manufacturer_id,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'status' => $this->status,
            'sort_order' => $this->sort_order,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'sku', $this->sku])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
    
    public function getName()
    {
        return $this->text->name;
    }
    
    public function setName($val)
    {
        return $this->name = $val;
    }
}
