<?php

namespace modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%shop_category_text}}".
 *
 * @property integer $product_id
 * @property string $language
 * @property string $name
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 *
 * @property Product $category
 */
class ProductText extends \yii\db\ActiveRecord
{
    use \modules\main\traits\MultilanguageChildTrait;
    
    public function load($data, $formName = null)
    {
        $this->loadDefaultAttrText();
        return parent::load($data, $formName);
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_product_text}}';
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[Product::SCENARIO_CREATE_IN_ADMIN] = $scenarios[Product::SCENARIO_UPDATE_IN_ADMIN] = ['name', 'description', 'meta_title', 'meta_description', 'meta_keyword'/*, '!product_id'*/, '!language_id'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge($this->defaultRules(['name', 'description', 'language_id', 'meta_title', 'meta_description', 'meta_keyword']), [
            [['product_id'], 'required'],
            [['product_id'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            
            [['product_id', 'language_id'], 'unique', 'targetAttribute' => ['product_id', 'language_id'], 'message' => 'The combination of Product ID and Language has already been taken.'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge($this->defaultAttributeLabels(['name', 'description', 'language_id', 'meta_title', 'meta_description', 'meta_keyword']), [
            'product_id' => Yii::t('shopProduct', 'Product ID'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
