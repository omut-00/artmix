<?php

namespace modules\shop\models;

use Yii;
use yii\db\ActiveRecord;

class ProductToCategory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_product_to_category}}';
    }

    
}
