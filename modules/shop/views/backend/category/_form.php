<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\tabs\TabsX;
use yii\flags\Flags;

/* @var $this yii\web\View */
/* @var $model modules\shop\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>

    <?php $tabTextItems = [];
    foreach($model->stackTexts as $text){
        $tabTextItems[] = [
            'label' => Flags::widget([
                'flag' => $text->language->country,
                'type' => Flags::FLAT_24,
              ]) . ' ' . ArrayHelper::getValue($text, 'language.name_ascii'),
            'content' => $this->render('parts/_text', ['text' => $text, 'form' => $form])
        ];
    } ?>
 
    <?php echo TabsX::widget([
        'encodeLabels' => false,
        'items' => [
            [
                'label' => '<i class="fa fa-language"></i> ' . Yii::t('shopCategory', 'General'),
                'content' => TabsX::widget([
                    //'position' => TabsX::POS_LEFT,
                    'encodeLabels' => false,
                    'items' => $tabTextItems
                ])
            ],
            [
                'label' => '<i class="fa fa-th-list"></i> ' . Yii::t('shopCategory', 'Data'),
                'content' => $this->render('parts/_data', ['model' => $model, 'form' => $form, 'listCategories' => $listCategories])
            ]
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('shopCategory', 'Create') : Yii::t('shopCategory', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
