<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\shop\models\Category */

$this->title = Yii::t('shopCategory', 'Create Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shopCategory', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listCategories' => $listCategories,
    ]) ?>

</div>
