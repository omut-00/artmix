<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use modules\main\widgets\CategorySelectWidget;
/* @var $this yii\web\View */
/* @var $searchModel modules\shop\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shopCategory', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('shopCategory', 'Create Category'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php $CategorySelectWidget = new CategorySelectWidget(); ?>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn'
            ],
            [
                'attribute' => 'name',
                'value' => 'name',
            ],
            [
                'attribute' => 'parent_id',
                //'filter' => Category::find()->select(['name', 'id'])->indexBy('id')->column(),
                /*'filter' => $CategorySelectWidget::widget([
                    'model' => $searchModel,
                    'attribute' => 'parent_id'
                ]),
                'value' => function($model)use($CategorySelectWidget, $searchModel){
                    return $CategorySelectWidget->treeNameParent($searchModel->list, $model->parent_id);
                }*/
                'value' => function ($model) {
                    return empty($model->parent_id) ? '-' : $model->parent->text->name;
                },
            ],
            //'id',
            //'parent_id',
            // 'name',
            'slug',
            //'image_id',
            'sort_order',
            'status',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
