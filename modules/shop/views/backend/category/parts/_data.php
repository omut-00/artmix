<?php

/**
 * @author Sergey Artikbayev <https://vk.com/omut00> <omut-00@mail.ru>
 */

?>

<?= $form->field($model, 'parent_id')->widget(\modules\main\widgets\CategorySelectWidget::classname(), [
    'items' => $listCategories,
    'value' => $model->parent_id,
]); ?>

<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

<?php //= $form->field($model, 'image_id')->textInput() ?>

<?= $form->field($model, 'sort_order')->textInput(['type' => 'number', 'value' => 0]) ?>

<?php $model->status = $model::STATUS_ENABLED; ?>
<?= $form->field($model, 'status')->widget(\kartik\switchinput\SwitchInput::className(), [
    'value'=> true
]) ?>

<?php /*= $form->field($model, 'created_at')->textInput() ?>

<?= $form->field($model, 'updated_at')->textInput()*/ ?>