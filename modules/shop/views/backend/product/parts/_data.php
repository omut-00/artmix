<?php

/**
 * @author Sergey Artikbayev <https://vk.com/omut00> <omut-00@mail.ru>
 */

?>

<?= $form->field($model, 'manufacturer_id')->textInput() ?>

<?= $form->field($model, 'quantity')->textInput() ?>

<?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'sku')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'sort_order')->textInput(['type' => 'number', 'value' => 0]) ?>

<?php $model->status = $model::STATUS_ENABLED; ?>
<?= $form->field($model, 'status')->widget(\kartik\switchinput\SwitchInput::className(), [
    'value'=> true
]) ?>

<?php /*= $form->field($model, 'created_at')->textInput() ?>

<?= $form->field($model, 'updated_at')->textInput()*/ ?>