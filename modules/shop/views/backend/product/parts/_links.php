<?php

/**
 * @author Sergey Artikbayev <https://vk.com/omut00> <omut-00@mail.ru>
 */

?>

<?= $form->field($model, 'toCategoriesArray[]')->widget(\modules\main\widgets\CategorySelectWidget::classname(), [
    'items' => $categories,
    'value' => \yii\helpers\ArrayHelper::getColumn($model->toCategories, 'category_id'),
    'multiple' => true,
    'tags' => true,
]) ?>