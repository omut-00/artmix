<?php

/**
 * @author Sergey Artikbayev <https://vk.com/omut00> <omut-00@mail.ru>
 */

use dosamigos\ckeditor\CKEditor;
?>
<?= $form->field($text, '['.$text->language->language_id.']name')->textInput() ?>
<?= $form->field($text, '['.$text->language->language_id.']description')->widget(CKEditor::className(), [
    'options' => ['rows' => 6],
    'preset' => 'basic'
]) ?>

<?= $form->field($text, '['.$text->language->language_id.']meta_title')->textInput() ?>
<?= $form->field($text, '['.$text->language->language_id.']meta_description')->textarea(['rows' => '6']) ?>
<?= $form->field($text, '['.$text->language->language_id.']meta_keyword')->textInput() ?>
