<?php

/* 
Author: Sergey Artikbayev https://vk.com/omut00 omut-00@mail.ru
 */

namespace modules\user;

use common\components\ModuleInterface;
use yii\web\GroupUrlRule;

class Module extends \common\components\Module implements ModuleInterface
{
    public static $name = 'user';
    
    /**
     * Initializes the module.
     */
    public function init()
    {
        parent::init();
    }
    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param \app\components\Application $app
     */
    public function bootstrap($app)
    {
        $app->get('frontendUrlManager')->rules[] = new GroupUrlRule([
            'prefix' => 'user',
            'rules' => [
                '<username:\w+>' => 'default/index',
            ],
        ]);
        
        /*$app->getUrlManager()->addRules([
            //'user' => 'user/default/index',
        ],false);*/
        
        $app->i18n->translations['user'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@modules/user/messages',
        ];
        
        /*$menu = new \dmstr\widgets\Menu();
        $menu->on($menu::EVENT_BEFORE_RUN, function(){
            echo '222';
            exit;
        });*/
        
        parent::bootstrap($app);
    }
    
    public function backendBootstrap($app)
    {
        // добавление пункта меню
        \yii\base\Event::on(\dmstr\widgets\Menu::className(), \dmstr\widgets\Menu::EVENT_BEFORE_RUN, function ($event) {
            $event->sender->items[] = ['label' => 'Пользователи', 'icon' => 'shopping-cart', 'url' => ['/user/default/index']];
        });
    }
    
    public function frontendBootstrap($app)
    {
        
    }
    
    /*public static function t($category, $message, $params = [], $language = null)
    {
        return \Yii::t('' . $category, $message, $params, $language);
    }*/
}