<?php

namespace modules\user\controllers\frontend;

use yii\web\Controller;
use modules\user\models\User;

/**
 * Default controller for the `product` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($username)
    {
        $user = User::findModel(['username' => $username]);
        
        return $this->render('index', [
            'user' => $user,
        ]);
    }
    public function findModel()
    {    
        if ($user) {
            return $user;
        }
        
        
    }
}
