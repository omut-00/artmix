<?php

/* 
Author: Sergey Artikbayev https://vk.com/omut00 omut-00@mail.ru
 */
namespace modules\user\models;

use Yii;
use yii\db\ActiveRecord;

class User extends ActiveRecord
{
    const SCENARIO_EDIT_IN_ADMIN = 'update_in_admin';

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_EDIT_IN_ADMIN] = ['username', 'email'];
        return $scenarios;
    }
    
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('user', 'Username'),
            'email' => Yii::t('user', 'Email'),
        ];
    }
    
    public function rules()
    {
        return [
            // username rules
            [['username'], 'trim'],
            [['username'], 'required'],
            [['username'], 'string', 'min' => 3, 'max' => 255],
            [['username'], 'match', 'pattern' => '/^\w+$/u', 'except' => 'social', 'message' => Yii::t('user', '{attribute} can contain only letters, numbers, and "_"')],
            [['username'], 'unique', 'message' => Yii::t('user', 'This username has already been taken')],
            
            // email rules
            [['email'], 'trim'],
            [['email'], 'required'],
            [['email'], 'email'],
            [['email'], 'string', 'min' => 3, 'max' => 255],
            [['email'], 'unique', 'message' => Yii::t('user', 'This email address has already been taken')],
        ];
    }
    
    public function findModel($id)
    {
        if( ($user = self::findOne($id)) ){
            return $user;
        }
        
        throw new NotFoundHttpException('The requested user does not exist.');
    }
}