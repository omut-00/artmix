<?php

/* 
Author: Sergey Artikbayev https://vk.com/omut00 omut-00@mail.ru
 */
namespace modules\user\models;

use yii\base\Model;
use modules\user\models\User;

class UserSearch extends Model
{
    /*public static function getUsers(){
        return $this->find();
    }*/
    public static function search()
    {
        $query = User::find();
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);
        
        return $dataProvider;
    }
}