<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('user', 'Update {modelClass}: ', [
  'modelClass' => 'User',
]) . ' ' . $user->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('user', 'Update {userName}', ['userName' => $user->username]);
?>
<div class="user-update">
    <div class="user-form">

        <?php $form = ActiveForm::begin([

        ]); ?>

        <?= $form->field($user, 'email')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($user, 'username')->textInput(['maxlength' => 255]) ?>

        <div class="form-group">
            <?= Html::submitButton($user->isNewRecord ? Yii::t('user', 'Create') : Yii::t('user', 'Update'), ['class' => $user->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>