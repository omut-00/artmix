<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

$this->title = $user->username;
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?= DetailView::widget([
    'model' => $user,
    'attributes' => [
        'email:email',
        'username',
    ],
]) ?>